**Rosetta Gitlab Sample Python CI Project Documentation** : -

Stages performed: - 1. Build stage
                    2. Sonar-qube check
                    3. Publish artifacts
        

**NOTE**:-Before start the build we have to maintain all required variables & runner setup.  See below variables & runner setup for project

Take all the variables and save as a secrets in gitlab variables as 
SONAR_TOKEN -  *********   ( Your sonarqube token ID )
SONAR_HOST_URL -  ******** ( Your sonarqube host url  )
JFROG_REGISTRY_URL - ******* ( Your Jfrog URL, you will find in your jfrog page ) JFROG_REGISTRY_USER - ****** ( Your j frog user id )
JFROG_REGISTRY_PASSWORD - ****** ( Your jfrog password ) 


**We have to install gitlab runner in one server and register with that to our gitlab.**
Ssh - My Gitlab runner tag.
Reference Documents : -
https://docs.gitlab.com/runner/install/linux-repository.html



**Stage : Build Stage : -**

Using Python as a service I had built the image using jfrog as a container registry.

build:
  stage: build 
  tags:
    - rosetta-docker
  script:
    - python3 myfile.py
  artifacts:
    #name: "$CI_JOB_NAME"
    name: build
    paths:
     - myfile.*



**Reference for yaml file & code setup :-**
https://gitlab.rosetta.ericssondevops.com/p.rajasekar/python-with-sonar.git     


python:latest - My Image name
CI_PIPELINE_IID - The project-level IID (internal ID) of the current pipeline. This ID is unique only within the current project.


**Refernce documents : -**
https://www.youtube.com/watch?v=HCuBdbuJdTU  (reference for build image )
https://sonarqube.rosetta.ericssondevops.com  (reference for sonarqube test repository )
https://artifactory.rosetta.ericssondevops.com/artifactory/sd-mana-isd-devops-generic ( reference for jfrog repository)



**Stage : Sonarqube check : -**

code_review:
  stage: code_review
  tags:
    - rosetta-docker
  image: 
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  variables:
    SONAR_TOKEN: "${CI_PROJECT_DIR}/.sonar"  # Defines the       location of the analysis task cache
    SONAR_HOST_URL: "https://sonarqube.rosetta.ericssondevops.com"
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  cache:
    key: "${CI_JOB_NAME}"
    paths:
      - .sonar/cache
  script: 
    - sonar-scanner -X  #-X -Dsonar.findbugs.allowuncompiledcode
  allow_failure: true
  only:
    - main  # or the name of your main branch


**Stage :- Publish artifacts : -**

publish_artifacts:
  stage: publish_artifacts
  tags:
    - rosetta-docker
  image: 
    name: meisterplan/jfrog-cli
    entrypoint: [""]
  variables:
    ARTIFACTORY_BASE_URL: https://artifactory.rosetta.ericssondevops.com/artifactory/sd-mana-isd-devops-generic
    REPO_NAME: rosetta-gitlab-demo
    ARTIFACT_NAME: build
    GIT_DEPTH: "0"
  script:
    - jfrog rt c --url="$ARTIFACTORY_BASE_URL"/ --apikey="$ARTIFACTORY_KEY"
    - jfrog rt u "build*" "$REPO_NAME"/"$ARTIFACT_NAME"_"$CI_PIPELINE_ID.zip" --recursive=false





 
**Gitlab Sample Python CI Project Documentation : -**

Stages performed: - 1. Build stage
                    2. test
                    3. release
                    4. Deploy 

**Python3 - My Image name**
CI_PIPELINE_IID - The project-level IID (internal ID) of the current pipeline. This ID is unique only within the current project.


**NOTE**:-Before start the build we have to maintain all required variables & runner setup.  See below variables & runner setup for project

We have to install gitlab runner in one server and register with that to our gitlab.

Ssh - My Gitlab runner tag.
Reference Documents : -
https://docs.gitlab.com/runner/install/linux-repository.html



**Stage : Build Stage : -**

build:
  stage: build
  tags:
    - docker
  script:
    - pip install build
    - python -m build -o . --wheel
  artifacts:
    paths:
      - ./*.whl

Reference for yaml file & code setup :-

 https://gitlab.com/raja1231/test-jfrog   





**Stage : test : -**

test:
  stage: test
  tags:
    - docker
  script:
    - pip install .
    - gitlab-download-artifacts . build 
    - ls -l *.whl


**Stage : release : -**

release this commit:
  when: manual
  stage: release
  only: [main]
  tags:
    - docker
  before_script:
    - git config --global user.email "$GITLAB_USER_EMAIL"
    - git config --global user.name "$GITLAB_USER_NAME"
  script:
    - pip install git-versioner
    - git-versioner --tag
    # Requires suitable project access token (or personal token on free gitlab.com) in protected env var: GIT_TOKEN
    - git push --tags http://root:$GIT_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git HEAD:main





**Stage : deploy : -**

deploy:
  stage: deploy
  tags:
    - docker
  script:
    - pip install .
    - pip install -U twine setuptools wheel gitlab-release
    - gitlab-download-artifacts . build








